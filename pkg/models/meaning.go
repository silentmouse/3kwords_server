package models

type Meaning struct {
	Noun      []MeaningItem
	Verb      []MeaningItem
	Adjective []MeaningItem
	Adverb    []MeaningItem
}

type MeaningItem struct {
	Title      string
	Definition string
	Synonyms   []Word
}

type Synonyms struct {
	Title string
	ID    uint
}
