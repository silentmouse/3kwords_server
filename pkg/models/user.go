package models

type User struct {
	Model
	Name               string
	Email              string
	FirstName          string
	LastName           string
	Phone              string
	Img                string
	Level              int
	WordCountNextLevel int
	AllWordsOnLevel    int
	WordsMiddle        int
	WordsLow           int
	WordsHigh          int
	Password           string
}

type UserWords struct {
	High   int
	Middle int
	Low    int
}
