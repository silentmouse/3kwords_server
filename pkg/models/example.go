package models

type Example struct {
	Noun      []Sentence
	Verb      []Sentence
	Adjective []Sentence
	Adverb    []Sentence
}

