package models

type Word struct {
	Model
	Title         string
	Transcription string
	Translation   string
	Level         int
	Audio         string
	Lang          string
	MetaNumber    int
	Frequency     float64
}

type WordsWords struct {
	Model
	FirstWordID  uint
	SecondWordID uint
	PartOfSpeech string
	Kind         string
	Definition   string
}

type Theme struct {
	Model
	Title string
}

type ThemesWords struct {
	Model
	ThemeID uint
	WordID  uint
}

type IrregularVerb struct {
	Model
	Title         string
	WordID        uint
	Time          string
	Order         int
	Number        int
	Transcription string
}

type SentencesWords struct {
	Model
	SentenceID   uint
	WordID       uint
	WordsWordsID uint
}

type Sentence struct {
	Model
	Text       string
	Lang       string
	Definition string `sql:"-"`
}

type SessionWords struct {
	Model
	UserID uint
	Number int
}

type CheckWord struct {
	Model
	UserID   uint
	WordID   uint
	Error    int
	Good     int
	Complete bool
}

type Exercise struct {
	Model
	SessionID uint
	UserID    uint
	WordID    uint
	Good      int
	Error     int
	Complete  bool
	Place     int
	Words     []Word
	Progress  float64 `sql:"-"`
}
