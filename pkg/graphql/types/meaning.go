package types

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/actions"
	"bitbucket.org/silentmouse/3kwords_server/pkg/models"
	"bitbucket.org/silentmouse/3kwords_server/pkg/utils"
	"github.com/graphql-go/graphql"
)

func MeaningType(ac *actions.MainObject) *graphql.Object {

	var typeVar *graphql.Object

	hashgr := utils.RandToken5()

	typeVar = graphql.NewObject(graphql.ObjectConfig{
		Name:        "Example_" + hashgr,
		Description: "example",
		Fields: graphql.Fields{
			"noun": &graphql.Field{
				Type:        graphql.NewList(MeaningItemType(ac)),
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Meaning); ok {
						return obj.Noun, nil
					}
					return nil, nil
				},
			},
			"verb": &graphql.Field{
				Type:        graphql.NewList(MeaningItemType(ac)),
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Meaning); ok {
						return obj.Verb, nil
					}
					return nil, nil
				},
			},
			"adjective": &graphql.Field{
				Type:        graphql.NewList(MeaningItemType(ac)),
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Meaning); ok {
						return obj.Adjective, nil
					}
					return nil, nil
				},
			},
			"adverb": &graphql.Field{
				Type:        graphql.NewList(MeaningItemType(ac)),
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Meaning); ok {
						return obj.Adverb, nil
					}
					return nil, nil
				},
			},
		},
	})

	return typeVar
}


func MeaningItemType(ac *actions.MainObject) *graphql.Object {

	var typeVar *graphql.Object

	hashgr := utils.RandToken5()

	typeVar = graphql.NewObject(graphql.ObjectConfig{
		Name:        "MeaningItem_" + hashgr,
		Description: "MeaningItem",
		Fields: graphql.Fields{
			"title": &graphql.Field{
				Type:        graphql.String,
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.MeaningItem); ok {
						return obj.Title,nil
					}
					return nil, nil
				},
			},
			"definition": &graphql.Field{
				Type:        graphql.String,
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.MeaningItem); ok {
						return obj.Definition, nil
					}
					return nil, nil
				},
			},
			"synonyms": &graphql.Field{
				Type:        graphql.NewList(LinkWordType(ac)),
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.MeaningItem); ok {
						return obj.Synonyms, nil
					}
					return nil, nil
				},
			},
		},
	})

	return typeVar
}


