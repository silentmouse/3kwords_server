package types

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/actions"
	"bitbucket.org/silentmouse/3kwords_server/pkg/models"
	"bitbucket.org/silentmouse/3kwords_server/pkg/utils"
	"github.com/graphql-go/graphql"
	"strconv"
)

func SessionWordsType(ac *actions.MainObject) *graphql.Object {

	var typeVar *graphql.Object

	hashgr := utils.RandToken5()

	typeVar = graphql.NewObject(graphql.ObjectConfig{
		Name:        "SessionWords_" + hashgr,
		Description: "SessionWords",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.SessionWords); ok {
						return obj.ID, nil
					}
					return nil, nil
				},
			},
			"number": &graphql.Field{
				Type:        graphql.String,
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.SessionWords); ok {
						return obj.Number, nil
					}
					return nil, nil
				},
			},
			"user_id": &graphql.Field{
				Type:        graphql.String,
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.SessionWords); ok {
						return obj.UserID, nil
					}
					return nil, nil
				},
			},
			"words": &graphql.Field{
				Type:        graphql.NewList(WordType(ac)),
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.SessionWords); ok {
						return ac.GetWordsFromSessionWords(obj), nil
					}
					return nil, nil
				},
			},

		},
	})

	return typeVar
}

func ExerciseType(ac *actions.MainObject) *graphql.Object {

	var typeVar *graphql.Object

	hashgr := utils.RandToken5()

	typeVar = graphql.NewObject(graphql.ObjectConfig{
		Name:        "Exercise_" + hashgr,
		Description: "Exercise",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Exercise); ok {
						return obj.ID, nil
					}
					return nil, nil
				},
			},
			"place": &graphql.Field{
				Type:        graphql.String,
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Exercise); ok {
						return obj.Place, nil
					}
					return nil, nil
				},
			},
			"word": &graphql.Field{
				Type:        WordType(ac),
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Exercise); ok {
						word_id := strconv.FormatUint(uint64(obj.WordID),10)
						mi := map[string]interface{}{"id": word_id}
						params := graphql.ResolveParams{Args: mi}
						return ac.GetWord(params), nil
					}
					return nil, nil
				},
			},
			"good": &graphql.Field{
				Type:        graphql.Int,
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Exercise); ok {
						return obj.Good, nil
					}
					return nil, nil
				},
			},
			"error": &graphql.Field{
				Type:        graphql.Int,
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Exercise); ok {
						return obj.Error, nil
					}
					return nil, nil
				},
			},
			"complete": &graphql.Field{
				Type:        graphql.Boolean,
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Exercise); ok {
						return obj.Complete, nil
					}
					return nil, nil
				},
			},
			"words": &graphql.Field{
				Type:        graphql.NewList(WordType(ac)),
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Exercise); ok {
						return obj.Words, nil
					}
					return nil, nil
				},
			},
			"progress": &graphql.Field{
				Type:        graphql.Float,
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Exercise); ok {
						return obj.Progress, nil
					}
					return nil, nil
				},
			},



		},
	})

	return typeVar
}
