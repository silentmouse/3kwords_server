package types

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/actions"
	"bitbucket.org/silentmouse/3kwords_server/pkg/models"
	"bitbucket.org/silentmouse/3kwords_server/pkg/utils"
	"github.com/graphql-go/graphql"
)

func TestType(ac *actions.MainObject) *graphql.Object {

	var typeVar *graphql.Object

	hashgr := utils.RandToken5()

	typeVar = graphql.NewObject(graphql.ObjectConfig{
		Name:        "Message_" + hashgr,
		Description: "message",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.NewNonNull(graphql.String),
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Test); ok {
						return obj.ID, nil
					}
					return nil, nil
				},
			},
			"name": &graphql.Field{
				Type:        graphql.String,
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Test); ok {
						return obj.Name, nil
					}
					return nil, nil
				},
			},
		},
	})

	return typeVar
}
