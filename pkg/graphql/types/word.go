package types

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/actions"
	"bitbucket.org/silentmouse/3kwords_server/pkg/models"
	"bitbucket.org/silentmouse/3kwords_server/pkg/utils"
	"github.com/graphql-go/graphql"
)

func WordType(ac *actions.MainObject) *graphql.Object {

	var typeVar *graphql.Object

	hashgr := utils.RandToken5()

	typeVar = graphql.NewObject(graphql.ObjectConfig{
		Name:        "Word_" + hashgr,
		Description: "word",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Word); ok {
						return obj.ID, nil
					}
					return nil, nil
				},
			},
			"title": &graphql.Field{
				Type:        graphql.String,
				Description: "title",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Word); ok {
						return obj.Title, nil
					}
					return nil, nil
				},
			},
			"transcription": &graphql.Field{
				Type:        graphql.String,
				Description: "transcription",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Word); ok {
						return obj.Transcription, nil
					}
					return nil, nil
				},
			},
			"translation": &graphql.Field{
				Type:        graphql.String,
				Description: "translation",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Word); ok {
						return ac.GetTranslation(obj)
					}
					return nil, nil
				},
			},
			"level": &graphql.Field{
				Type:        graphql.Int,
				Description: "translation",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Word); ok {
						return obj.Level, nil
					}
					return nil, nil
				},
			},
			"audio": &graphql.Field{
				Type:        graphql.Int,
				Description: "translation",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Word); ok {
						return obj.Audio, nil
					}
					return nil, nil
				},
			},
			"meanings": &graphql.Field{
				Type:        MeaningType(ac),
				Description: "translation",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Word); ok {
						return ac.GetMeaning(obj), nil
					}
					return nil, nil
				},
			},
			"examples": &graphql.Field{
				Type:        ExampleType(ac),
				Description: "translation",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Word); ok {
						return ac.GetExample(obj), nil
					}
					return nil, nil
				},
			},

		},
	})

	return typeVar
}


func LinkWordType(ac *actions.MainObject) *graphql.Object {

	var typeVar *graphql.Object

	hashgr := utils.RandToken5()

	typeVar = graphql.NewObject(graphql.ObjectConfig{
		Name:        "LinkWordType_" + hashgr,
		Description: "LinkWordType",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Word); ok {
						return obj.ID, nil
					}
					return nil, nil
				},
			},
			"title": &graphql.Field{
				Type:        graphql.String,
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Word); ok {
						return obj.Title, nil
					}
					return nil, nil
				},
			},
		},
	})

	return typeVar
}
