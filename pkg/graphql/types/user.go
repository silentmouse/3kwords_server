package types

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/actions"
	"bitbucket.org/silentmouse/3kwords_server/pkg/models"
	"bitbucket.org/silentmouse/3kwords_server/pkg/utils"
	"github.com/graphql-go/graphql"
)

func UserType(ac *actions.MainObject) *graphql.Object {

	var typeVar *graphql.Object

	hashgr := utils.RandToken5()

	typeVar = graphql.NewObject(graphql.ObjectConfig{
		Name:        "Word_" + hashgr,
		Description: "word",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.User); ok {
						return obj.ID, nil
					}
					return nil, nil
				},
			},
			"first_name": &graphql.Field{
				Type:        graphql.String,
				Description: "title",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.User); ok {
						return obj.FirstName, nil
					}
					return nil, nil
				},
			},
			"last_name": &graphql.Field{
				Type:        graphql.String,
				Description: "title",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.User); ok {
						return obj.LastName, nil
					}
					return nil, nil
				},
			},
			"email": &graphql.Field{
				Type:        graphql.String,
				Description: "title",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.User); ok {
						return obj.LastName, nil
					}
					return nil, nil
				},
			},
			"name": &graphql.Field{
				Type:        graphql.String,
				Description: "title",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.User); ok {
						return obj.Name, nil
					}
					return nil, nil
				},
			},
			"phone": &graphql.Field{
				Type:        graphql.String,
				Description: "title",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.User); ok {
						return obj.Phone, nil
					}
					return nil, nil
				},
			},
			"words": &graphql.Field{
				Type:        UserWordsType(),
				Description: "title",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.User); ok {
						return ac.GetUserWords(obj)
					}
					return nil, nil
				},
			},
			"level": &graphql.Field{
				Type:        graphql.Int,
				Description: "title",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.User); ok {
						return obj.Level, nil
					}
					return nil, nil
				},
			},
			"word_count_next_level": &graphql.Field{
				Type:        graphql.Int,
				Description: "title",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.User); ok {
						return obj.WordCountNextLevel, nil
					}
					return nil, nil
				},
			},
			"all_words_on_level": &graphql.Field{
				Type:        graphql.Int,
				Description: "title",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.User); ok {
						return obj.AllWordsOnLevel, nil
					}
					return nil, nil
				},
			},


		},
	})

	return typeVar
}

func UserWordsType() *graphql.Object {

	var typeVar *graphql.Object

	hashgr := utils.RandToken5()

	typeVar = graphql.NewObject(graphql.ObjectConfig{
		Name:        "Word_" + hashgr,
		Description: "word",
		Fields: graphql.Fields{
			"high": &graphql.Field{
				Type:        graphql.Int,
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.UserWords); ok {
						return obj.High, nil
					}
					return nil,nil
				},
			},
			"middle": &graphql.Field{
				Type:        graphql.Int,
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.UserWords); ok {
						return obj.Middle, nil
					}
					return nil,nil
				},
			},
			"low": &graphql.Field{
				Type:        graphql.Int,
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.UserWords); ok {
						return obj.Low, nil
					}
					return nil,nil
				},
			},

		},
	})

	return typeVar
}

