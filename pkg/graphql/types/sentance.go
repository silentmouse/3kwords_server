package types

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/actions"
	"bitbucket.org/silentmouse/3kwords_server/pkg/models"
	"bitbucket.org/silentmouse/3kwords_server/pkg/utils"
	"github.com/graphql-go/graphql"
)

func SentanceType(ac *actions.MainObject) *graphql.Object {

	var typeVar *graphql.Object

	hashgr := utils.RandToken5()

	typeVar = graphql.NewObject(graphql.ObjectConfig{
		Name:        "Message_" + hashgr,
		Description: "message",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.NewNonNull(graphql.String),
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Sentence); ok {
						return obj.ID, nil
					}
					return nil, nil
				},
			},
			"text": &graphql.Field{
				Type:        graphql.String,
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Sentence); ok {
						return obj.Text, nil
					}
					return nil, nil
				},
			},
			"definition": &graphql.Field{
				Type:        graphql.String,
				Description: "The id of the message",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if obj, ok := p.Source.(models.Sentence); ok {
						return obj.Definition, nil
					}
					return nil, nil
				},
			},
		},
	})

	return typeVar
}
