package mutations

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/actions"
	"bitbucket.org/silentmouse/3kwords_server/pkg/graphql/types"
	"github.com/graphql-go/graphql"
)

func (fields *MyGraphFields) AddSession(ac *actions.MainObject) *MyGraphFields{

	fields.Fields["createSessionWords"] = &graphql.Field{
		Type:        types.SessionWordsType(ac),
		Description: "create new comment",
		Args: graphql.FieldConfigArgument{
			"user_id": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"number": &graphql.ArgumentConfig{
				Type: graphql.Int,
			},
			"kind": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			return ac.CreateSessionWords(params), nil
		},
	}

	fields.Fields["changeExercise"] = &graphql.Field{
		Type:        types.ExerciseType(ac),
		Description: "create new comment",
		Args: graphql.FieldConfigArgument{
			"id": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"kind": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			return ac.ChangeExercise(params), nil
		},
	}

	return fields

}