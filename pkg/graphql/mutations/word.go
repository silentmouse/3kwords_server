package mutations

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/actions"
	"bitbucket.org/silentmouse/3kwords_server/pkg/graphql/types"
	"github.com/graphql-go/graphql"
	"strconv"
)

func (fields *MyGraphFields) AddWord(ac *actions.MainObject) *MyGraphFields{

	fields.Fields["addWord"] = &graphql.Field{
		Type:        types.WordType(ac),
		Description: "create new comment",
		Args: graphql.FieldConfigArgument{
			"title": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"transcription": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			"translation": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			"audio": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			"level": &graphql.ArgumentConfig{
				Type: graphql.Int,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			return ac.AddWord(params), nil
		},
	}

	fields.Fields["updateWord"] = &graphql.Field{
		Type:        types.WordType(ac),
		Description: "create new comment",
		Args: graphql.FieldConfigArgument{
			"id": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"title": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			"transcription": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			"translation": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			"audio": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			"level": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			return ac.UpdateWord(params), nil
		},
	}

	fields.Fields["createSessionWords"] = &graphql.Field{
		Type:        types.WordType(ac),
		Description: "create new comment",
		Args: graphql.FieldConfigArgument{
			"user_id": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"number": &graphql.ArgumentConfig{
				Type: graphql.Int,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			return ac.CreateSessionWords(params), nil
		},
	}

    fields.Fields["getUserWordsByKind"] = &graphql.Field{
		Type:        types.WordType(ac),
		Description: "create new comment",
		Args: graphql.FieldConfigArgument{
			"user_id": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"kind": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			us_id,_ := strconv.ParseUint(params.Args["user_id"].(string),10,64)
			return ac.GetUserWordsByKind(uint(us_id), params.Args["kind"].(string))
		},
	}

	return fields

}