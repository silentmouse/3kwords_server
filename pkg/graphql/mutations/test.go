package mutations

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/actions"
	"bitbucket.org/silentmouse/3kwords_server/pkg/graphql/types"
	"github.com/graphql-go/graphql"
)

func (fields *MyGraphFields) AddTest(ac *actions.MainObject) *MyGraphFields{

	fields.Fields["test"] = &graphql.Field{
		Type:        types.TestType(ac),
		Description: "create new comment",
		Args: graphql.FieldConfigArgument{
			"name": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			return ac.AddTest(params), nil
		},
	}

	return fields

}