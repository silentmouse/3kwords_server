package kschema

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/actions"
	"bitbucket.org/silentmouse/3kwords_server/pkg/graphql/mutations"
	"bitbucket.org/silentmouse/3kwords_server/pkg/graphql/queries"
	"github.com/Sirupsen/logrus"
	"github.com/graphql-go/graphql"
)

var (
	KSchema graphql.Schema
)

type MyGraphFields struct {
	Fields graphql.Fields
}

func InitSchema(ac *actions.MainObject) {

	logrus.Infoln("Comeon!")

	mutation_fields := mutations.MyGraphFields{Fields: graphql.Fields{}}

	mutation_fields.AddTest(ac).AddWord(ac).AddUser(ac).AddSession(ac)

	var rootMutation = graphql.NewObject(graphql.ObjectConfig{
		Name: "RootMutation",
		Fields: mutation_fields.Fields,
	})

	fields := queries.MyGraphFields{Fields: graphql.Fields{}}

	fields.AddTest(ac).AddWords(ac).AddUsers(ac).AddSessions(ac)

	queryType := graphql.NewObject(graphql.ObjectConfig{
		Name: "Query",
		Fields: fields.Fields,
	})

	KSchema, _ = graphql.NewSchema(graphql.SchemaConfig{
		Query:    queryType,
		Mutation: rootMutation,
	})
}

