package queries

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/actions"
	"bitbucket.org/silentmouse/3kwords_server/pkg/graphql/types"
	"github.com/graphql-go/graphql"
)

func (fields *MyGraphFields) AddUsers(ac *actions.MainObject) *MyGraphFields {

	fields.Fields["user"] = &graphql.Field{
		Type:        types.UserType(ac),
		Description: "create new comment",
		Args: graphql.FieldConfigArgument{
			"id": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			return ac.GetUser(params), nil
		},
	}

	fields.Fields["users"] = &graphql.Field{
		Type:        graphql.NewList(types.UserType(ac)),
		Description: "create new comment",
		Args: graphql.FieldConfigArgument{
			"limit": &graphql.ArgumentConfig{
				Type: graphql.Int,
			},
			"offset": &graphql.ArgumentConfig{
				Type: graphql.Int,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			return ac.GetUsers(params), nil
		},
	}

	return fields

}
