package queries

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/actions"
	"bitbucket.org/silentmouse/3kwords_server/pkg/graphql/types"
	"github.com/graphql-go/graphql"
)

func (fields *MyGraphFields) AddWords(ac *actions.MainObject) *MyGraphFields {

	fields.Fields["words"] = &graphql.Field{
		Type:        graphql.NewList(types.WordType(ac)),
		Args: graphql.FieldConfigArgument{
			"limit": &graphql.ArgumentConfig{
				Type: graphql.Int,
			},
			"offset": &graphql.ArgumentConfig{
				Type: graphql.Int,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			return ac.GetWords(params), nil
		},
	}

	fields.Fields["getWordsFromList"] = &graphql.Field{
		Type:        graphql.NewList(types.WordType(ac)),
		Args: graphql.FieldConfigArgument{
			"user_id": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			"list_id": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			return ac.GetWordsFromList(params)
		},
	}

	fields.Fields["word"] = &graphql.Field{
		Type:        types.WordType(ac),
		Args: graphql.FieldConfigArgument{
			"id": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			"title": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			return ac.GetWord(params), nil
		},
	}

	return fields

}
