package queries

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/actions"
	"bitbucket.org/silentmouse/3kwords_server/pkg/graphql/types"
	"github.com/graphql-go/graphql"
)

func (fields *MyGraphFields) AddTest(ac *actions.MainObject) *MyGraphFields{

	fields.Fields["tests"] = &graphql.Field{
		Type:        graphql.NewList(types.TestType(ac)),
		Description: "create new comment",
		Args: graphql.FieldConfigArgument{
			"text": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			return ac.GetTest(params), nil
		},
	}

	return fields

}