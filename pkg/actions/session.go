package actions

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/models"
	"github.com/graphql-go/graphql"
	"math/rand"
	"strconv"
	"time"
)

func (mo *MainObject) CreateSessionWords(p graphql.ResolveParams) (session models.SessionWords) {

	var words []models.Word
	var check_words []models.CheckWord

	us_id, _ := strconv.ParseUint(p.Args["user_id"].(string), 10, 64)

	mo.DB.Raw("select * from check_words where user_id = ?", us_id).Scan(&check_words)

	ids := []uint{}

	for _, v := range check_words {
		ids = append(ids, v.WordID)
	}

	if len(ids) > 0{
	   mo.DB.Raw("select * from words where lang = 'en' and id not in (?) limit 10", ids).Scan(&words)
	}else{
		mo.DB.Raw("select * from words where lang = 'en' limit 10").Scan(&words)
	}

	if p.Args["kind"] != nil{
		if p.Args["kind"].(string) == "low"{
			words,_ = mo.GetUserWordsByKind(uint(us_id), "low")
		}
		if p.Args["kind"].(string) == "middle"{
			words,_ = mo.GetUserWordsByKind(uint(us_id), "middle")
		}
		if p.Args["kind"].(string) == "high"{
			words,_ = mo.GetUserWordsByKind(uint(us_id), "high")
		}
	}

	session.UserID = uint(us_id)
	session.Number = 1

	mo.DB.Create(&session)

	for i, v := range words {
		var ex models.Exercise
		ex.WordID = v.ID
		ex.SessionID = session.ID
		ex.UserID = uint(us_id)
		ex.Place = i
		mo.DB.Create(&ex)
	}

	return session
}

func (mo *MainObject) GetWordsFromSessionWords(session models.SessionWords) (words []models.Word) {

	mo.DB.Raw("select * from words where id in "+
		"(select word_id from exercises where session_id = ?)", session.ID).Scan(&words)

	return words
}

func (mo *MainObject) GetSessionWords(p graphql.ResolveParams) (session models.SessionWords) {

	mo.DB.First(&session, p.Args["id"].(string))

	return session
}

func (mo *MainObject) GetExercise(p graphql.ResolveParams) (exercise models.Exercise) {

	mo.DB.Raw("select * from exercises where session_id = ? and complete = false "+
		"order by place limit 1", p.Args["session_words_id"].(string)).Scan(&exercise)

	var complete_count int
	var all_count int

	mo.DB.Table("exercises").Where("session_id = ? ", p.Args["session_words_id"].(string)).Count(&all_count)
	mo.DB.Table("exercises").Where("session_id = ? and complete = true", p.Args["session_words_id"].(string)).Count(&complete_count)

	fcc := float64(complete_count)
	fac := float64(all_count)

	exercise.Progress = fcc / fac

	var words []models.Word

	mo.DB.Raw("select * from words where id in "+
		"(select word_id from exercises where session_id = ?)", p.Args["session_words_id"].(string)).Scan(&words)

	if exercise.Progress == 1 {
		go mo.CalculatewordsForUser(p.Args["session_words_id"].(string))
	}

	exercise.Words = Shuffle(words)

	return exercise
}

func (mo *MainObject) CalculatewordsForUser(session_id string) {

	var exercises []models.Exercise

	mo.DB.Raw("select * from exercises where session_id = ?", session_id).Scan(&exercises)

	for _, v := range exercises {
		var check_word models.CheckWord
		mo.DB.Raw("select * from check_words where word_id = ? and user_id = ?", v.WordID, v.UserID).Scan(&check_word)

		if check_word.ID == 0 {
			check_word.WordID = v.WordID
			check_word.UserID = v.UserID
			check_word.Error = v.Error
			check_word.Good = v.Good
			check_word.Complete = v.Complete
			mo.DB.Create(&check_word)
		} else {
			check_word.Error = v.Error
			check_word.Good = v.Good
			check_word.Complete = v.Complete
			mo.DB.Save(&check_word)
		}
	}

	var user models.User

	mo.DB.First(&user, exercises[0].UserID)

	go mo.UpdateUserLevelInfo(user)

}

func (mo *MainObject) GetExercises(p graphql.ResolveParams) (exercises []models.Exercise) {

	mo.DB.Raw("select * from exercises where session_id = ? "+
		"order by place", p.Args["session_words_id"].(string)).Scan(&exercises)

	return exercises
}

func (mo *MainObject) ChangeExercise(p graphql.ResolveParams) (exercise models.Exercise) {

	mo.DB.Raw("select * from exercises where id = ?", p.Args["id"].(string)).Scan(&exercise)

	switch p.Args["kind"].(string) {
	case "complete":
		exercise.Complete = true
	case "error":
		exercise.Error = exercise.Error + 1
	case "good":
		exercise.Good = exercise.Good + 1
		exercise.Complete = true
	}
	exercise.Place = exercise.Place + 10
	mo.DB.Save(&exercise)
	return exercise
}

func Shuffle(slice []models.Word) []models.Word {
	r := rand.New(rand.NewSource(time.Now().Unix()))

	res := slice

	for n := len(slice); n > 0; n-- {
		randIndex := r.Intn(n)
		res[n-1], res[randIndex] = res[randIndex], res[n-1]
	}

	return res
}


