package actions

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/models"
	"github.com/graphql-go/graphql"
	"strconv"
)

func (mo *MainObject) AddWord(p graphql.ResolveParams) (word models.Word) {

	word.Title = p.Args["title"].(string)
	if p.Args["translation"] != nil {
		word.Translation = p.Args["translation"].(string)
	}
	if p.Args["transcription"] != nil {
		word.Transcription = p.Args["transcription"].(string)
	}
	if p.Args["level"] != nil {
		word.Level = p.Args["level"].(int)
	}
	if p.Args["audio"] != nil {
		word.Audio = p.Args["audio"].(string)
	}

	mo.DB.Create(&word)

	return word
}

func (mo *MainObject) UpdateWord(p graphql.ResolveParams) (word models.Word) {

	mo.DB.First(&word, p.Args["id"].(string))

	if p.Args["title"] != nil {
		word.Title = p.Args["title"].(string)
	}
	if p.Args["translation"] != nil {
		word.Translation = p.Args["translation"].(string)
	}
	if p.Args["transcription"] != nil {
		word.Transcription = p.Args["transcription"].(string)
	}
	if p.Args["level"] != nil {
		word.Level = p.Args["level"].(int)
	}
	if p.Args["audio"] != nil {
		word.Audio = p.Args["audio"].(string)
	}

	mo.DB.Create(&word)

	return word
}

func (mo *MainObject) GetWordsFromList(p graphql.ResolveParams) (words []models.Word, error error) {

	var check_words []models.CheckWord

	us_id, _ := strconv.ParseUint(p.Args["user_id"].(string), 10, 64)

	mo.DB.Raw("select * from check_words where user_id = ?", us_id).Scan(&check_words)

	ids := []uint{}

	for _, v := range check_words {
		ids = append(ids, v.WordID)
	}

	if len(ids) > 0{
	    mo.DB.Raw("select * from words where lang = 'en' and id not in (?) limit 10", ids).Scan(&words)
	}else{
		mo.DB.Raw("select * from words where lang = 'en' limit 10", ids).Scan(&words)
	}


	return words,nil

}

func (mo *MainObject) GetWords(p graphql.ResolveParams) (words []models.Word) {

	limit := 10
	offset := 0

	if p.Args["limit"] != nil {
		limit = p.Args["limit"].(int)
	}

	if p.Args["offset"] != nil {
		offset = p.Args["offset"].(int)
	}

	mo.DB.Raw("select * from words where lang = 'en' order by title limit ? offset ? ", limit, offset).Scan(&words)

	return words
}

func (mo *MainObject) GetWord(p graphql.ResolveParams) (word models.Word) {

	if p.Args["title"] != nil {
		mo.DB.Raw("select * from words where title = ?", p.Args["title"].(string)).Scan(&word)
		return word
	}

	mo.DB.First(&word, p.Args["id"].(string))

	return word
}

func (mo *MainObject) GetTranslation(word models.Word) (string, error) {

	var res_word models.Word

	var ww models.WordsWords

	mo.DB.Raw("select second_word_id from words_words where first_word_id = ? and kind = 'en_ru'", word.ID).Scan(&ww)

	mo.DB.First(&res_word, ww.SecondWordID)

	return res_word.Title, nil
}

func (mo *MainObject) GetUserWords(user models.User) (user_words models.UserWords, error error) {

    var cwords []models.CheckWord

    mo.DB.Raw("select * from check_words where user_id = ?", user.ID).Scan(&cwords)

    for _, v := range cwords{
    	if (v.Error >= v.Good * 3){
    		user_words.Low = user_words.Low + 1
		}
		if ((v.Error >= v.Good * 2 || v.Error == v.Good) && v.Error < v.Good * 3){
			user_words.Middle = user_words.Middle + 1
		}
		if (v.Error < v.Good ){
			user_words.High = user_words.High + 1
		}
	}

	return user_words,nil
}


func (mo *MainObject) GetUserWordsByKind(user_id uint, kind string) (words []models.Word, error error) {

	var cwords []models.CheckWord

	mo.DB.Raw("select * from check_words where user_id = ?", user_id).Scan(&cwords)

	for _, v := range cwords{
		var word models.Word
		if (v.Error >= v.Good * 3 && kind == "low"){
			mo.DB.First(&word, v.WordID)
			words = append(words, word)
		}
		if ((v.Error >= v.Good * 2 || v.Error == v.Good) && v.Error < v.Good * 3 && kind == "middle"){
			mo.DB.First(&word, v.WordID)
			words = append(words, word)
		}
		if (v.Error < v.Good && kind == "high"){
			mo.DB.First(&word, v.WordID)
			words = append(words, word)
		}
	}

	return words,nil
}