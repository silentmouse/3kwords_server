package actions

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/models"
	"errors"
	"github.com/graphql-go/graphql"
)

func (mo *MainObject) RegUser(p graphql.ResolveParams) (user models.User, error error) {

	//mo.DB.Raw("select * from users where phone = ? ", p.Args["phone"].(string)).Scan(&user)
	mo.DB.Raw("select * from users where email = ? ", p.Args["email"].(string)).Scan(&user)
	if user.ID != 0 {
		return user, errors.New("user already exist")
	}

	user.Name = p.Args["first_name"].(string) + " " + p.Args["last_name"].(string)
	if p.Args["first_name"] != nil {
		user.FirstName = p.Args["first_name"].(string)
	}
	if p.Args["last_name"] != nil {
		user.LastName = p.Args["last_name"].(string)
	}
	if p.Args["phone"] != nil {
		user.Phone = p.Args["phone"].(string)
	}
	if p.Args["email"] != nil {
		user.Email = p.Args["email"].(string)
	}
	if p.Args["password"] != nil {
		user.Password = p.Args["password"].(string)
	}

	mo.DB.Create(&user)

	return user, nil
}

func (mo *MainObject) LoginUser(p graphql.ResolveParams) (user models.User, error error) {

	//mo.DB.Raw("select * from users where phone = ? ", p.Args["phone"].(string)).Scan(&user)
	mo.DB.Raw("select * from users where email = ? and password = ?", p.Args["email"].(string), p.Args["password"].(string)).Scan(&user)
	if user.ID != 0 {
		return user, nil
	} else {
		return user, errors.New("wrong login or password")
	}
}

func (mo *MainObject) AddUser(p graphql.ResolveParams) (user models.User) {

	//mo.DB.Raw("select * from users where phone = ? ", p.Args["phone"].(string)).Scan(&user)
	mo.DB.Raw("select * from users where email = ? ", p.Args["email"].(string)).Scan(&user)
	if user.ID != 0 {
		return user
	}

	user.Name = p.Args["first_name"].(string) + " " + p.Args["last_name"].(string)
	if p.Args["first_name"] != nil {
		user.FirstName = p.Args["first_name"].(string)
	}
	if p.Args["last_name"] != nil {
		user.LastName = p.Args["last_name"].(string)
	}
	if p.Args["phone"] != nil {
		user.Phone = p.Args["phone"].(string)
	}
	if p.Args["email"] != nil {
		user.Email = p.Args["email"].(string)
	}

	mo.DB.Create(&user)

	return user
}

func (mo *MainObject) UpdateUser(p graphql.ResolveParams) (user models.User) {

	mo.DB.First(&user, p.Args["id"].(string))

	if p.Args["first_name"] != nil {
		user.FirstName = p.Args["first_name"].(string)
	}
	if p.Args["last_name"] != nil {
		user.LastName = p.Args["last_name"].(string)
	}
	if p.Args["phone"] != nil {
		user.Phone = p.Args["phone"].(string)
	}

	user.Name = p.Args["first_name"].(string) + " " + p.Args["last_name"].(string)

	mo.DB.Save(&user)

	return user
}

func (mo *MainObject) GetUsers(p graphql.ResolveParams) (users []models.User) {

	mo.DB.Find(&users)

	return users
}

func (mo *MainObject) GetUser(p graphql.ResolveParams) (user models.User) {

	mo.DB.First(&user, p.Args["id"].(string))

	return user
}

func (mo *MainObject) UpdateUserLevelInfo(user models.User) {

	var count int
	var count_point int

	mo.DB.Table("check_words").Where("user_id = ? and complete = true",user.ID).Count(&count)

	for i, v := range LevelsEnglish {
		if i < count && count < (v["count_words"]+i) {
			count_point = i
		}
	}

	if count_point == 0 {
		user.AllWordsOnLevel = 300
		user.WordCountNextLevel = 300 - count
		user.Level = 0
	} else {
		user.AllWordsOnLevel = LevelsEnglish[count_point]["count_words"]
		user.WordCountNextLevel = user.AllWordsOnLevel + count_point - count
		user.Level = LevelsEnglish[count_point]["level"]
	}

	mo.DB.Save(&user)

}
