package actions

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/models"
	"github.com/Sirupsen/logrus"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"os"
)

type MainObject struct{
	DB     *gorm.DB
}


func (mo *MainObject) New(){
	var addr string

	val, ok := os.LookupEnv("DB_HOST")
	if !ok {
		addr = "postgresql://root@localhost:26257/kwords?sslmode=disable"
	} else {
		addr = val
	}
	var err error
	mo.DB, err = gorm.Open("postgres", addr)
	if err != nil {
		logrus.Fatal(err)
	}

	mo.AddTables()
}


func (mo *MainObject) AddTables(){

	mo.DB.AutoMigrate(models.Test{})
	mo.DB.AutoMigrate(models.Word{})
	mo.DB.AutoMigrate(models.User{})
	mo.DB.AutoMigrate(models.CheckWord{})
	mo.DB.AutoMigrate(models.Exercise{})
	mo.DB.AutoMigrate(models.SessionWords{})
	mo.DB.AutoMigrate(models.WordsWords{})
	mo.DB.AutoMigrate(models.Sentence{})
	mo.DB.AutoMigrate(models.SentencesWords{})
	mo.DB.AutoMigrate(models.Theme{})
	mo.DB.AutoMigrate(models.ThemesWords{})
	mo.DB.AutoMigrate(models.IrregularVerb{})

}



