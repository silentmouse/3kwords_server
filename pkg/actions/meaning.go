package actions

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/models"
)

func (mo *MainObject) GetMeaning(word models.Word) (models.Meaning) {

	meaning := models.Meaning{}

	mo.addPartOfSpeechTitle(&meaning,word,"noun")
	mo.addPartOfSpeechTitle(&meaning,word,"verb")
	mo.addPartOfSpeechTitle(&meaning,word,"adjective")
	mo.addPartOfSpeechTitle(&meaning,word,"adverb")

	return meaning
}

func (mo *MainObject) addPartOfSpeechTitle(meaning *models.Meaning,word models.Word,part_of_speech string) {

	var meaningItems []models.MeaningItem

	var synonyms []models.Word

	var words_words []models.WordsWords

	mo.DB.Raw("select * from words_words where first_word_id = ? " +
		"and part_of_speech = ? and kind ='synonym'", word.ID,part_of_speech).Scan(&words_words)

	ids_ww := []uint{}

	for _, v := range words_words{
		ids_ww = append(ids_ww, v.SecondWordID)
	}

	mo.DB.Raw("select * from words where id in (?)",ids_ww).Scan(&synonyms)


	for _, v :=range words_words{
		ch := findMeaningItem(meaningItems,v.Definition)
		if ch == -1{
			var mi models.MeaningItem
			mi.Definition = v.Definition
			index := FindWord(synonyms,v.SecondWordID)
			mi.Synonyms = []models.Word{synonyms[index]}
			meaningItems = append(meaningItems,mi)
		}else{
			index := FindWord(synonyms,v.SecondWordID)
			meaningItems[ch].Synonyms = append(meaningItems[ch].Synonyms,synonyms[index])
		}
	}

	for i,v := range meaningItems{
		meaningItems[i].Title = mo.addTitle(v.Synonyms, part_of_speech)
	}

	switch part_of_speech {
	  case "noun":
		  meaning.Noun = meaningItems
	case "adjective":
		meaning.Adjective = meaningItems
	case "verb":
		meaning.Verb = meaningItems
	case "adverb":
		meaning.Adverb = meaningItems
	}






}


func (mo *MainObject) addTitle(synonyms []models.Word, part_of_speech string) (string){
	ids := []uint{}
	var ww []models.WordsWords

	for _,v := range synonyms{
		ids = append(ids, v.ID)
	}

	mo.DB.Raw("select * from words_words where kind = 'en_ru' " +
		"and first_word_id in (?) and part_of_speech = ?",ids,part_of_speech).Scan(&ww)

	if len(ww) == 0{
		return "zagl"
	}

	ids = []uint{}

	for _,v := range ww{
		ids = append(ids, v.SecondWordID)
	}

	var word models.Word

	mo.DB.Raw("select * from words where id in (?)",ids).Scan(&word)

	return word.Title

}


func findMeaningItem(mi []models.MeaningItem,definition string) (int){
	for i, v := range mi{
		if v.Definition == definition{
			return i
		}
	}

	return -1

}

func FindWord(words []models.Word,id uint) (int){
	for i, v := range words{
		if v.ID == id{
			return i
		}
	}
	return -1
}
