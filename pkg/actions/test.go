package actions

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/models"
	"github.com/graphql-go/graphql"
)

func (mo *MainObject) AddTest(p graphql.ResolveParams) (test models.Test){

	test.Name = p.Args["name"].(string)

	mo.DB.Create(&test)

	return test
}


func (mo *MainObject) GetTest(p graphql.ResolveParams) (tests []models.Test){

	mo.DB.Find(&tests)

	return tests
}