package actions

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/models"
)

func (mo *MainObject) GetExample(word models.Word) (models.Example) {

	example := models.Example{}

	mo.addPartOfSpeechTitleExamples(&example,word,"noun")
	mo.addPartOfSpeechTitleExamples(&example,word,"verb")
	mo.addPartOfSpeechTitleExamples(&example,word,"adjective")
	mo.addPartOfSpeechTitleExamples(&example,word,"adverb")

	return example
}

func (mo *MainObject) addPartOfSpeechTitleExamples(example *models.Example,word models.Word,part_of_speech string) {

	var words_words []models.WordsWords

	mo.DB.Raw("select * from words_words where first_word_id = ? " +
		"and part_of_speech = ? and kind ='synonym'", word.ID,part_of_speech).Scan(&words_words)

	ids_ww := []uint{}

	for _, v := range words_words{
		ids_ww = append(ids_ww, v.ID)
	}

	var sww []models.SentencesWords

	mo.DB.Raw("select * from sentences_words where words_words_id in (?)", ids_ww).Scan(&sww)

	ids_ww = []uint{}

	for _, v := range sww{
		ids_ww = append(ids_ww, v.SentenceID)
	}

	var sentences []models.Sentence

	mo.DB.Raw("select * from sentences where id in (?)",ids_ww).Scan(&sentences)

	for i, v :=range sentences{
		sw_index := FindSentenceWords(sww,v.ID)
		ww_index := FindWordsWords(words_words,sww[sw_index].WordsWordsID)
		sentences[i].Definition = words_words[ww_index].Definition
	}

	switch part_of_speech {
	case "noun":
		example.Noun = sentences
	case "adjective":
		example.Adjective = sentences
	case "verb":
		example.Verb = sentences
	case "adverb":
		example.Adverb = sentences
	}

}

func FindWordsWords(ss []models.WordsWords,id uint) (int){
	for i, v := range ss{
		if v.ID == id{
			return i
		}
	}
	return -1
}

func FindSentenceWords(ss []models.SentencesWords,id uint) (int){
	for i, v := range ss{
		if v.SentenceID == id{
			return i
		}
	}
	return -1
}
