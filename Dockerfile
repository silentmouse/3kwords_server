FROM golang:1.9.1

COPY main /go/bin/wb-inventory

CMD [ "wb-inventory" ]

EXPOSE 5000
