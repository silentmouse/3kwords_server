#!/usr/bin/env bash

echo 'BEGIN COMPILE'
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .
echo 'COMPILE'
git add -A
git commit -m "fix"
git push origin master
echo 'PUSHED'

WORK_DIR=$(mktemp -d)

function cleanup {
  rm -rf "$WORK_DIR"
  echo "Deleted temp working directory $WORK_DIR"
}

trap cleanup EXIT

NAMESPACE=dev
COMMIT=$(git rev-parse HEAD)
git archive HEAD | tar -x -C ${WORK_DIR}

cd ${WORK_DIR}

NAME=silentmouse/kwords-server
VER="${COMMIT::6}"
TAG="$NAME:latest"

docker build -t ${TAG} .
docker push ${TAG}

