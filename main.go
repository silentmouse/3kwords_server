package main

import (
	"bitbucket.org/silentmouse/3kwords_server/pkg/actions"
	"bitbucket.org/silentmouse/3kwords_server/pkg/graphql"
	_ "bitbucket.org/silentmouse/3kwords_server/pkg/models"
	"github.com/Sirupsen/logrus"
	"github.com/graphql-go/handler"
	"net/http"
	"context"
)

var (MO actions.MainObject)

func main(){

	MO.New()

	kschema.InitSchema(&MO)

	h := handler.New(&handler.Config{
		Schema:   &kschema.KSchema,
		Pretty:   true,
		GraphiQL: true,
	})

	logrus.Println("Now server is running on port 5000 bum")
	logrus.Println("Test with Get      : curl -g 'http://localhost:5000/graphql?query={spheres{name}}'")
	middleware := add_headers(h)
	http.Handle("/graphql", middleware)


	http.ListenAndServe(":5000", nil)

}

func add_headers(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authorizationHeader := r.Header.Get("X-Token")
		//p := []byte{}
		//r.Body.Read(p)
		//tmp_body, _:= r.GetBody()
		//body , _ := ioutil.ReadAll(tmp_body)
		//logrus.Printf("request == %s ", string(body))
		//logrus.Infoln("request == ", r.GetBody)
		entitiesHeader := r.Header.Get("X-Entites")
		if origin := r.Header.Get("Origin"); origin != "" {
			w.Header().Set("Access-Control-Allow-Origin", origin)
		}
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "X-Token, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, X-Entites")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		ctx := context.WithValue(r.Context(), "auth", authorizationHeader)
		ctx = context.WithValue(ctx, "entities", entitiesHeader)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}